import os
def menu():
    os.system('cls')
    print('1) registrar productos')
    print('2) mostrar el listado de productos')
    print('3) mostrar productos [desde, hasta] ')
    print('4) sume X al stock de todos los productos cuyo valor actual de stock sea menor a Y')
    print('5) Eliminar todos los productos cuyo stock sea igual a cero')
    print('6) Fin')
    opcion = int(input('Ingrese una opción: '))
    while not(opcion >= 1 and opcion <= 6): 
        opcion = int(input('Ingrese una opción: '))
    return opcion
def pulsarTecla():
    input('Pulse un tecla...')
def registrarproductos():
    print('Cargar productos')
    productos = {}
    cod = -1
    while (cod != 0):
         cod = int(input('codigo (cero para finalizar): '))
         if cod != 0: 
            
            if cod not in productos:    
                descripcion = input('Nombre: ')
                precio = ingreseprecio()
                stock=ingresestock()
                productos[cod] = [descripcion,precio,stock]
                print('agregado correctamente')
            else:
                print('el codigo ya existe')

    return productos
def ingreseprecio():
    precio=float(input('precio: '))
    while not(precio>=0):
         precio=float(input('precio: '))
    return precio
def ingresestock():
    stock=int(input('stock: '))
    while not(stock>=0):
         stock=int(input('stock: '))
    return stock
def mostrar(diccionario):
    print('Listado de productos')
    for clave, valor in diccionario.items():
        print(clave,valor)
def  productosDesde_Hasta (diccionario, desde, hasta):
    for clave, valor in diccionario.items():
        if (valor[2] >=desde) and (valor[2] <=hasta):
         print(clave,valor)
def  sumaXstock (diccionario, x, y):
    for clave, valor in diccionario.items():
        if (valor[2] < y):
            valor =+ x 
            print(clave,valor)
def  eliminarproducto(diccionario):
     for clave, valor in diccionario.items():
         if(valor[2] == 0):
             del diccionario[valor]
             print(clave,valor)

#Principal
opcion = 0

while opcion != 6:
    opcion = menu()
    if opcion == 1: 
        productos= registrarproductos()
        pulsarTecla()
    elif opcion == 2: 
        mostrar(productos)
    elif opcion == 3:
        desde = int(input('stock desde: '))
        hasta = int(input('stock hasta: '))
        print(productosDesde_Hasta (productos, desde, hasta))    
        pulsarTecla()
    elif opcion == 4:
        x = int(input('cantidad que desea agregar al stock: '))
        y = int(input('valor limite de stock : '))
        print(sumaXstock (productos, x, y))    
        pulsarTecla()      
    elif opcion == 5:
        eliminarproducto(productos)      
        pulsarTecla()
    elif opcion == 6: 
        print('Fin del programa') 