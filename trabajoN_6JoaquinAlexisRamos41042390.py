import os
from io import open
dic={}
fichero=open('vehiculos.txt','r',encoding="utf-8")
fichero.readline()
for linea in fichero:
    dominio,marca,tipo,modelo,kilometraje,preval,preven,estado = linea.strip().split(";")
    dic[dominio]=[marca,tipo,modelo,kilometraje,preval,preven,estado]
diccionario=dic    
def menu():
    os.system('cls')
    print('1) agregar vehiculos')
    print('2) buscar un automovil')
    print('3) buscar un automovil por su marca ')
    print('4) guardar vehiculos de una sola marca')
    print('5) fin')
    opcion = int(input('Ingrese una opción: '))
    while not(opcion >= 1 and opcion <= 5): 
        opcion = int(input('Ingrese una opción: '))
    return opcion
def pulsarTecla():
    input('Pulse un tecla...')     
def cargarvehiculos(dic):
    respuesta = 's'
    while respuesta == 's':
        dominio =verificardominio()
        marca =tipomarca()
        tipo = tipoautomovil()
        modelo =verificarmodelo()
        kilometraje= float(input('ingrese su kilometraje'))
        preval=int(input('ingrese modelo o año de su vehiculo: '))
        preven=(preval//10)+preval
        estado='Disponible'
        dic[dominio]=[ marca, tipo, modelo, kilometraje,preval,preven,estado]
        respuesta = input('Continuar?: ')
    return dic
def verificardominio():
    dominio =str(input('ingrese su dominio: '))
    while not(len(dominio)) >=6 and (len(dominio))<=9:
        dominio =str(input('ingrese su dominio: '))
    return dominio
def tipomarca():
    marca = input('Nombre de la marca: (R=Renault, F=Ford, C=Citroen, F=Fiat')
    if marca =='R' or marca=='r' :
       marcaa='renault'
    elif marca=='F' or marca=='f':
       marcaa='ford'
    elif marca=='C' or marca=='c':
       marcaa='citroen'
    elif marca=='F' or marca=='f':
       marcaa='Fiat'
    return marcaa
def tipoautomovil():
    tipo = input('tipo de automovil : U=Utilitario, A=Automóvil ')
    if tipo =='U' or tipo=='u' :
       tipoa='Utilitario'
    elif tipo=='A' or tipo=='a':
       tipoa='Automovil'
    return tipoa
def verificarmodelo():
    modelo =int(input('ingrese su modelo entre 2005 hasta 2020: '))
    while not modelo >=2005 and modelo<=2020:
        modelo =int(input('ingrese su modelo entre 2005 hasta 2020: '))
    return modelo
def preciovaluado():
    preciovaluado=int(input('ingrese modelo o año de su vehiculo: '))
    estado=int(input("ingrese estado de su vehiculo / 1:buen estado /2:mal estado" ))
    if preciovaluado>=2005 and preciovaluado<2010 and estado==1:
        x=400000+100000
    elif preciovaluado>=2005 and preciovaluado<2010 and estado==2:
        x=400000-50000 
    elif preciovaluado>=2010 and preciovaluado<2015 and estado==1:
        x=800000+100000
    elif preciovaluado>=2010 and preciovaluado<2015 and estado==2:
        x=800000-50000
    elif preciovaluado>=2015 and preciovaluado<=2020 and estado==1:
        x=1400000+100000
    elif preciovaluado>=2015 and preciovaluado<=2020 and estado==2:   
        x=1400000-50000
    return x
def buscarautomovil(diccionario):
    marca = buscarmarca()
    pos=-1
    for i ,item in enumerate(diccionario):
        if item[1] == marca:  
            pos = i
            if pos==-1:
               print('no existe esa marca en nuestros registros')
            else:
               print(item[0],item[1],item[2],item[3],item[4],item[5],item[6],item[7])
def buscarmarca():
    marca = (input('ingrese marca del vehiculo: '))
    if marca=='ren'or marca=='rena'or marca=='renau'or marca=='renaul'or marca=='renault':
        auto='Renault'
    elif marca=='fo'or marca=='for'or marca=='ford':
        auto='Ford'
    elif marca=='citr'or marca=='citro'or marca=='citroe'or marca=='citroen':
        auto='Citroen'
    elif marca=='fi'or marca=='fia'or marca=='fiat':
        auto='Fiat'
    return auto
def busqueda_binaria(diccionario, valor):
    primero=0
    ultimo= len(diccionario[0]) -1
    encontrado=False
    while primero <= ultimo and not encontrado:
        mitad = (primero + ultimo)//2
        if diccionario[mitad]==valor:
            encontrado=True
        else:
            if valor < diccionario[mitad]:
                ultimo = mitad - 1
            else:
                primero = mitad + 1
    return encontrado 
def mostrar(diccionario):
   
    for clave, valor in diccionario.items():
        print(clave,valor)
def nuevofichero(diccionario):
    marca = buscarmarca()
    for clave, valor in diccionario.items():
        if valor[1]==marca:
           
           archivo=open('autos.txt','w',encoding='utf-8')
           autos={}
           autos=clave,valor
           archivo.write(autos)
           archivo.close
           
#Principal
opcion = 0
while opcion != 5: 
    opcion = menu()
    if opcion == 1:
        cargarvehiculos(diccionario)
        mostrar(diccionario)
        pulsarTecla()
    elif opcion == 2:
        valor=str(input('ingrese dominio'))
        busqueda_binaria(diccionario,valor)
        pulsarTecla()
    elif opcion == 3: 
         buscarautomovil(diccionario)
         pulsarTecla()   
    elif opcion == 4:
         nuevofichero(diccionario)    
         pulsarTecla()   
    elif opcion == 5: 
         pulsarTecla()   
         print('Fin del programa...') 